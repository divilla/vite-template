import { createRouter, createWebHistory } from 'vue-router';
import NProgress from 'nprogress';
import exceptionRoutes from './route-exception.js';
import asyncRoutes from './route-async.js';
import commonRoutes from './route-common.js';

const routes = [...commonRoutes, ...exceptionRoutes, ...asyncRoutes];

const router = createRouter({
  history: createWebHistory('/'),
  routes,
  scrollBehavior: () => ({ left: 0, top: 0 }),
});

router.beforeEach((to, from) => {
  console.log('Routing：to, from\n', to, from);
  document.title = to.meta.title || 'Journey Tester';
  if (!NProgress.isStarted()) {
    NProgress.start();
  }
});

router.afterEach((to, from) => {
  console.log('Routing：to, from\n', to, from);
  NProgress.done();
});

export function setupRouter(app) {
  app.use(router);
}
