const asyncRoutes = [
  {
    path: '/',
    name: 'home',
    meta: {
      title: '',
      icon: '',
    },
    component: () => import('@/views/home/index.vue'),
  },
  {
    path: '/hello-world',
    name: 'hello-world',
    meta: {
      title: 'Template configuration process',
      icon: '',
    },
    component: () => import('@/views/example/HelloWorld.vue'),
  },
];

export default asyncRoutes;
