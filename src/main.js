import { createI18n } from 'vue-i18n';
import messages from '@intlify/vite-plugin-vue-i18n/messages';
import { createApp } from 'vue';
import App from './App.vue';
import { setupRouter } from './router/index';
import { setupStore } from './store/index';

import 'virtual:windi.css';
// Devtools: https://windicss.org/integrations/vite.html#design-in-devtools
import 'virtual:windi-devtools';
import '@/assets/styles/index.scss';

const i18n = createI18n({
  locale: 'en',
  messages,
});

async function setupApp() {
  const app = createApp(App);
  setupStore(app);
  setupRouter(app);
  app.use(i18n);
  app.mount('#app', true);
}

setupApp();
