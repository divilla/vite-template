import { defineConfig, loadEnv } from 'vite';
import { resolve } from 'path';
import presets from './presets/presets';

export default defineConfig((env) => {
  const viteEnv = loadEnv(env.mode, `.env.${env.mode}`);
  return {
    base: viteEnv.VITE_BASE,
    plugins: [presets(env)],
    // plugins: [
    //   vue(),
    //   eslintPlugin(),
    //   Unocss(presetUno(), presetAttributify(), presetIcons()),
    //   ViteFonts({
    //     google: {
    //       families: ['Montserrat', 'sans-serif'],
    //     },
    //   }),
    // ],
    resolve: {
      alias: {
        '@': resolve(__dirname, './src'), // 把 @ 指向到 src 目录去
      },
    },
    // server: {
    //   host: true,
    //   port: 8080,
    //   open: true,
    //   cors: true,
    //   strictPort: true,
    //   proxy: {
    //     '/api': {
    //       target: 'http://localhost:8080/',
    //       changeOrigin: true,
    //       rewrite: (path) => path.replace('/api/', '/'),
    //     },
    //   },
    // },
    build: {
      brotliSize: false,
      chunkSizeWarningLimit: 2000,
      terserOptions: {
        compress: {
          drop_console: true,
          drop_debugger: true,
        },
      },
      assetsDir: 'static/assets',
      rollupOptions: {
        output: {
          chunkFileNames: 'static/js/[name]-[hash].js',
          entryFileNames: 'static/js/[name]-[hash].js',
          assetFileNames: 'static/[ext]/[name]-[hash].[ext]',
        },
      },
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `
            @import "@/assets/styles/variables.scss";
          `,
          javascriptEnabled: true,
        },
      },
    },
  };
});
